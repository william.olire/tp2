package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class ListViewModel extends AndroidViewModel {
    private BookRepository repository;
    private LiveData<List<Book>> allBooks;
    private MutableLiveData<Book> searchResults;

    public ListViewModel (Application application) {
        super(application);
        repository = new BookRepository(application);
        allBooks = repository.getAllBooks();
        searchResults = repository.getSearchResults();
    }

    MutableLiveData<List<Book>> getSearchResults() {
        return getSearchResults();
    }
    LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }
    public void insertBook(Book book) {
        repository.insertBook(book);
    }
    public void findProduct(long id) {
        repository.findBook(id);
    }
    public void deleteProduct(long id) {
        repository.deleteBook(id);
    }
}
